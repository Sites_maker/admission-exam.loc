<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\Extenders\References as REF;


class CreateConsultationRequestsTable extends Migration {
    
    public function up() {
        
        Schema::create(REF::DB_TABLES['consultRequests']['tableName'], function (Blueprint $table) {
            
            $table -> integer(REF::DB_TABLES['consultRequests']['fieldNames']['visitId']) -> unique();
            
            $table -> string(REF::DB_TABLES['consultRequests']['fieldNames']['potentialCustomerName']) -> nullable(false);
            $table -> string(REF::DB_TABLES['consultRequests']['fieldNames']['potentialCustomerEmail']) -> nullable(false);
            
            $table -> string(REF::DB_TABLES['consultRequests']['fieldNames']['clickedButtonId']) -> nullable(false);
            $table -> timestampTz(REF::DB_TABLES['consultRequests']['fieldNames']['requestDate']) -> nullable(false);
        });
    }

    public function down() {
        Schema::dropIfExists(REF::DB_TABLES['consultRequests']['tableName']);
    }
}