<?php

namespace App\Http\Controllers\Open;
use App\Http\Controllers\Controller;

use Mail;
use DB;
use Carbon\Carbon;

use App\Extenders\References as REF;
use App\Extenders\Notifier;


class FeedbackController extends Controller {

    private $inputtedData;
    
    private static $actualCarbonTimezone = 'Europe/Moscow';
    private static $carbonTimeFormate = 'Y-m-d H:i:s';
    private $feedackSentTime;
    
    private static $PATH_TO_SETTINGS_FILE;
    private $feedbackRequestSettings;
    
    private $activeFirstReplyMethod;
    private $waitingTimePeriodUntilAutoReply;
    private $emailsForSendingNotification;

    
    public function __construct() {
        
        self::$PATH_TO_SETTINGS_FILE = storage_path().REF::SUBPATHS['storage']['settings'];
        
        $settings = json_decode(file_get_contents(self::$PATH_TO_SETTINGS_FILE), true);
        $this -> feedbackRequestSettings = $settings['FeedbackRequest'];
        
        $this -> activeFirstReplyMethod = $this -> feedbackRequestSettings['FirstReplyMethod'];
        $this -> waitingTimePeriodUntilAutoReply = $this -> feedbackRequestSettings['WaitingTimePeriodUntilAutoReply'];
        $this -> emailsForSendingNotification = $this -> feedbackRequestSettings['NotifyAboutNewFeedbackRequestTo'];
    }
    
    public function submitConsultationRequest(){
        
        if ($this -> isFeedbackRecordWithSameVisitIdExistsInDatabase()) {
            Notifier::notifyAboutAttemptToSendFeedbackAgain();
            return redirect() -> route(REF::OPEN_ROUTES['top']) 
                    -> withErrors([REF::ERROR_MESSAGES['feedbackForm']['twoRequestsImpossible']]);
        }
        
        $this -> inputtedData = request() -> all();
        $this -> validateInputtedData();
        $this -> submitRequestEmailToAdmin();
                       
        if (count(Mail::failures()) === 0) {
            
            request() -> session() -> put(REF::SESSION_VARIABLES['feedbackSent'], true);
            $this -> addConsultationRequestDataToDataBase();
            return redirect() -> route(REF::OPEN_ROUTES['thanksForConsultationRequest']);
        }
    }
    
    private function isFeedbackRecordWithSameVisitIdExistsInDatabase(){
        
        $queryResult = DB::table(REF::DB_TABLES['consultRequests']['tableName']) -> where([
                REF::DB_TABLES['consultRequests']['fieldNames']['visitId'] => 
                    request() -> session() -> get(REF::SESSION_VARIABLES['visitId']),
            ]) -> first();
        
        return !is_null($queryResult) ? true : false;
    }

    private function validateInputtedData() {
      
        $errorMessages = REF::ERROR_MESSAGES['feedbackForm'];
            
        $this -> validate(request(), [
            REF::FORMS['feedbackForm']['fieldNames']['name'] => 'required|max:255',
            REF::FORMS['feedbackForm']['fieldNames']['email'] => 'required|email'
        ], $errorMessages);
    }
    
    private function submitRequestEmailToAdmin(){
        
        $this -> feedackSentTime =  Carbon::now(self::$actualCarbonTimezone) -> format(self::$carbonTimeFormate);
        
        Mail::send(REF::EMAIL_VIEWS['consultRequestNotification'], [
            'inputedData' => $this -> inputtedData,
            'feedackSentTime' => $this -> feedackSentTime,
            'inputFieldNames' => REF::FORMS['feedbackForm']['fieldNames'],
            'activeFirstReplyMethod' => $this -> activeFirstReplyMethod
        ], function($message) {
                
                $subject = 'MyLanding-Studio.ru: Новый запрос на консультацию';
                
                $robotEmail = env('NOTIFICATIOIN_ROBOT_EMAIL');
                $message -> from($robotEmail, 'Оповещения MyLanding-Studio.ru');
                
                $adminEmail = env('ADMIN_EMAIL');
                $message->to($adminEmail)->subject($subject);
                
                $message->to($adminEmail)->subject($this -> inputtedData[REF::FORMS['feedbackForm']['fieldNames']['email']]);
            });
    }
    

    private function addConsultationRequestDataToDataBase(){
        
        DB::table(REF::DB_TABLES['consultRequests']['tableName']) -> insert([

            REF::DB_TABLES['consultRequests']['fieldNames']['visitId'] => 
               request() -> session() -> get(REF::SESSION_VARIABLES['visitId']),

            REF::DB_TABLES['consultRequests']['fieldNames']['potentialCustomerName'] => 
                $this -> inputtedData[REF::FORMS['feedbackForm']['fieldNames']['name']],

            REF::DB_TABLES['consultRequests']['fieldNames']['potentialCustomerEmail'] => 
                $this -> inputtedData[REF::FORMS['feedbackForm']['fieldNames']['email']],
            
            REF::DB_TABLES['consultRequests']['fieldNames']['clickedButtonId'] => 
                $this -> inputtedData[REF::FORMS['feedbackForm']['fieldNames']['buttonId']],

            REF::DB_TABLES['consultRequests']['fieldNames']['requestDate'] => 
                $this -> feedackSentTime,
        ]);
    }
}