<?php

namespace App\Http\Controllers\Open;

use App\Http\Controllers\Controller;
use DB;

use App\Extenders\References as REF;
use App\Extenders\Notifier;


class AuxiliaryPagesController extends Controller {
    
    public function renderNoJsRequestPage(){
        
        return view(REF::OPEN_VIEWS['noJsRequestForm']);
    }
    
    
    public function renderThanksForConsultationRequestPage(){
        
        if (!request() -> session() -> get(REF::SESSION_VARIABLES['feedbackSent']) || 
                request() -> session() -> get(REF::SESSION_VARIABLES['thanksPageVisited'])) { 
            
            Notifier::notifyAboutIllegalAttemptToRequestThanksPage();
            return redirect() -> route(REF::OPEN_ROUTES['top']);
        }
        
        request() -> session() -> put(REF::SESSION_VARIABLES['thanksPageVisited'], true);
        
        return view(REF::OPEN_VIEWS['thanks']);
    }
}