<!DOCTYPE html>
<html lang="ru">
    <head>

        <title></title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="Author" content="Вы">
        <meta name="description" content="">
        <meta name="Keywords" content="">
        <link rel="shortcut icon" type="image/x-icon" href="{{ asset('common/img/favicon.ico') }}">

        @yield('CssLinks')
        
        <?php /*
        * Обычно в head мы скрипты не подключаем; только если это какой-то частный 
        * случай типа кода от Google Analytics.
        */?>
        @yield('EndHeadJS')
    </head>

    <body>
        
        {{-- Шапка с меню --}}
        @include('open/widgets/RegularHeaderWithMenu')
        
        {{-- Основной контент (задаётся в унаследованных шаблонах ) --}}
        @yield('Content')
        
        {{-- Футер --}}
        @include('open/widgets/RegularFooter')
        
        {{-- Скрытые элементы типа модальных окон --}}
        @yield('HiddenElements')
        
        {{-- Подключение скриптов (должен быть только один файл) --}}
        @yield('EndBodyJS')
    </body>
</html>