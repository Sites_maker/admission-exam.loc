/* global __dirname */

const   path = require('path'),
            fs = require('fs');

class HPath {
        
    constructor(){

        const   PUG_COMMON_FOLDER_NAME = '1_pug',
                    SASS_COMMON_FOLDER_NAME = '2_sass',
                    ES6_COMMON_FOLDER_NAME = '3_es6',
                    PHP_COMMON_FOLDER_NAME = '4_php',

                    CSS_COMMON_FOLDER_NAME = 'css',
                    JS_COMMON_FOLDER_NAME = 'js',
                    PHP_OUTPUT_FOLDER_NAME = 'php',

                    FONTS_COMMON_DEVELOPMENT_FOLDER_NAME = 'fonts',
                    IMAGES_COMMON_DEVELOPMENT_FOLDER_NAME = 'images',
                    SOUNDS_COMMON_DEVELOPMENT_FOLDER_NAME = 'sounds',
                    STRINGS_COMMON_DEVELOPMENT_FOLDER_NAME = 'strings',
                    TEXTINGS_COMMON_DEVELOPMENT_FOLDER_NAME = 'textings',

                    FONTS_COMMON_PRODUCTION_FOLDER_NAME = FONTS_COMMON_DEVELOPMENT_FOLDER_NAME,
                    IMAGES_COMMON_PRODUCTION_FOLDER_NAME = 'img';


        this.folderNames = {

            framework: {
                root: 'hikari',
                    gulpTasks: 'GulpTasks',
                    jsExtenders: 'JsExtenders',
                    styles: 'Styles'
            },

            development: {

                root: 'development',

                source: {
                    root: '1_source',
                    common: {
                        root: '0_common',
                        pug: PUG_COMMON_FOLDER_NAME,
                        sass: SASS_COMMON_FOLDER_NAME,
                        es6: ES6_COMMON_FOLDER_NAME,
                        fonts: FONTS_COMMON_DEVELOPMENT_FOLDER_NAME,
                        images: IMAGES_COMMON_DEVELOPMENT_FOLDER_NAME,
                        sounds: SOUNDS_COMMON_DEVELOPMENT_FOLDER_NAME,
                        strings: STRINGS_COMMON_DEVELOPMENT_FOLDER_NAME,
                        textings: TEXTINGS_COMMON_DEVELOPMENT_FOLDER_NAME
                    },
                    open: {
                        root: '1_open',
                        pug: PUG_COMMON_FOLDER_NAME,
                        sass: SASS_COMMON_FOLDER_NAME,
                        es6: ES6_COMMON_FOLDER_NAME,
                        php: PHP_COMMON_FOLDER_NAME,
                        fonts: FONTS_COMMON_DEVELOPMENT_FOLDER_NAME,
                        images: IMAGES_COMMON_DEVELOPMENT_FOLDER_NAME,
                        sounds: SOUNDS_COMMON_DEVELOPMENT_FOLDER_NAME,
                        strings: STRINGS_COMMON_DEVELOPMENT_FOLDER_NAME,
                        textings: TEXTINGS_COMMON_DEVELOPMENT_FOLDER_NAME
                    },
                    admin: {
                        root: '2_admin',
                        pug: PUG_COMMON_FOLDER_NAME,
                        sass: SASS_COMMON_FOLDER_NAME,
                        es6: ES6_COMMON_FOLDER_NAME,
                        php: PHP_COMMON_FOLDER_NAME,
                        fonts: FONTS_COMMON_DEVELOPMENT_FOLDER_NAME,
                        images: IMAGES_COMMON_DEVELOPMENT_FOLDER_NAME,
                        sounds: SOUNDS_COMMON_DEVELOPMENT_FOLDER_NAME,
                        strings: STRINGS_COMMON_DEVELOPMENT_FOLDER_NAME,
                        textings: TEXTINGS_COMMON_DEVELOPMENT_FOLDER_NAME
                    },
                    development: {
                        root: '3_development',
                        pug: PUG_COMMON_FOLDER_NAME,
                        sass: SASS_COMMON_FOLDER_NAME,
                        es6: ES6_COMMON_FOLDER_NAME,
                        fonts: FONTS_COMMON_DEVELOPMENT_FOLDER_NAME,
                        images: IMAGES_COMMON_DEVELOPMENT_FOLDER_NAME,
                        sounds: SOUNDS_COMMON_DEVELOPMENT_FOLDER_NAME,
                        strings: STRINGS_COMMON_DEVELOPMENT_FOLDER_NAME,
                        textings: TEXTINGS_COMMON_DEVELOPMENT_FOLDER_NAME
                    }
                },

                devBuild: {
                    root: '2_devBuild',
                    common: {
                        root: 'common',
                        css: CSS_COMMON_FOLDER_NAME,
                        js: JS_COMMON_FOLDER_NAME,
                        images: IMAGES_COMMON_PRODUCTION_FOLDER_NAME,
                        fonts: FONTS_COMMON_PRODUCTION_FOLDER_NAME
                    },
                    open:{
                        root: 'open',
                        css: CSS_COMMON_FOLDER_NAME,
                        js: JS_COMMON_FOLDER_NAME,
                        php: PHP_OUTPUT_FOLDER_NAME,
                        images: IMAGES_COMMON_PRODUCTION_FOLDER_NAME,
                        fonts: FONTS_COMMON_PRODUCTION_FOLDER_NAME
                    },
                    admin: {
                        root: 'admin',
                        css: CSS_COMMON_FOLDER_NAME,
                        js: JS_COMMON_FOLDER_NAME,
                        php: PHP_OUTPUT_FOLDER_NAME,
                        images: IMAGES_COMMON_PRODUCTION_FOLDER_NAME,
                        fonts: FONTS_COMMON_PRODUCTION_FOLDER_NAME
                    },
                    development: {
                        root: 'development',
                        css: CSS_COMMON_FOLDER_NAME,
                        js: JS_COMMON_FOLDER_NAME,
                        images: IMAGES_COMMON_PRODUCTION_FOLDER_NAME,
                        fonts: FONTS_COMMON_PRODUCTION_FOLDER_NAME
                    }
                }
            },

            production: {
                root: 'public',
                common: {
                    root: 'common',
                    css: CSS_COMMON_FOLDER_NAME,
                    js: JS_COMMON_FOLDER_NAME,
                    images: IMAGES_COMMON_PRODUCTION_FOLDER_NAME,
                    fonts: FONTS_COMMON_PRODUCTION_FOLDER_NAME
                },
                open: { 
                    root: 'open',
                    css: CSS_COMMON_FOLDER_NAME,
                    js: JS_COMMON_FOLDER_NAME,
                    php: PHP_OUTPUT_FOLDER_NAME,
                    images: IMAGES_COMMON_PRODUCTION_FOLDER_NAME,
                    fonts: FONTS_COMMON_PRODUCTION_FOLDER_NAME
                },
                admin: {
                    root: 'admin',
                    css: CSS_COMMON_FOLDER_NAME,
                    js: JS_COMMON_FOLDER_NAME,
                    php: PHP_OUTPUT_FOLDER_NAME,
                    images: IMAGES_COMMON_PRODUCTION_FOLDER_NAME,
                    fonts: FONTS_COMMON_PRODUCTION_FOLDER_NAME
                }
            }
        };


        this.projectAbsolutePath = path.resolve(__dirname, '..');

        this.developmentBuildAbsolutePath = path.join(
                this.projectAbsolutePath, 
                this.folderNames.development.root,
                this.folderNames.development.devBuild.root);

        this.productionBuildAbsolutePath = path.join(
                this.projectAbsolutePath, 
                this.folderNames.production.root);

        this.laravelProductionBuildFilesSelectionWhichOkToRefresh = [
            this.productionBuildAbsolutePath +   '\\**\\*',
            '!' + this.productionBuildAbsolutePath + '\\.htaccess',
            '!' + this.productionBuildAbsolutePath + '\\index.php',
            '!' + this.productionBuildAbsolutePath +   '\\robots.txt'
        ];


        // на будущее: константу для директории тасков
        this.gulpTasksPaths = {

            pug2html: './' + 
                this.folderNames.framework.root + '/' + 
                this.folderNames.framework.gulpTasks + '/' + 'Pug2HTML',

            validateOpenHtmlPages: './' +
                this.folderNames.framework.root + '/' + 
                this.folderNames.framework.gulpTasks + '/' + 'ValidateHTML',

            provideFrameworkStyleDependences: './' +
                this.folderNames.framework.root + '/' + 
                this.folderNames.framework.gulpTasks + '/' + 'StyleDependences',

            styles: './' +
                this.folderNames.framework.root + '/' + 
                this.folderNames.framework.gulpTasks + '/' + 'Styles',

            webpack: './' +
                this.folderNames.framework.root + '/' + 
                this.folderNames.framework.gulpTasks + '/' + 'Webpack',

            images: './' +
                this.folderNames.framework.root + '/' + 
                this.folderNames.framework.gulpTasks + '/' + 'Images',

            fonts:  './' +
                this.folderNames.framework.root + '/' + 
                this.folderNames.framework.gulpTasks + '/' + 'Fonts',
            
            copyHtmlToProduction: path.join(
                this.projectAbsolutePath,
                this.folderNames.framework.root,
                this.folderNames.framework.gulpTasks,
                'CopyHtmlToProduction'),

            prepareEmailsInitialHtml: path.join(
                this.projectAbsolutePath,
                this.folderNames.framework.root,
                this.folderNames.framework.gulpTasks,
                'PrepareEmailsInitialHtml'),

            prepareEmailsFinalHtml: path.join(
                this.projectAbsolutePath,
                this.folderNames.framework.root,
                this.folderNames.framework.gulpTasks,
                'PrepareEmailsFinalHtml'),

            copyMockBackendFiledToBuild: path.join(
                this.projectAbsolutePath,
                this.folderNames.framework.root,
                this.folderNames.framework.gulpTasks,
                'CopyMockBackendFilesToBuild')
            };


        this.devBuildStartPageRelPathBase = this.buildUncPathFromFromFolderNames([
            this.folderNames.development.root,
            this.folderNames.development.devBuild.root
        ]);

        this.allDevbuildFileSelector = this.buildUncPathFromFromFolderNames([
            this.folderNames.development.root,
            this.folderNames.development.devBuild.root
        ]) + '\\**\\*.*';



        // HTML 関連

        const 

                PUG_COMMON_SOURCE_FILES_SELECTION_BASE =  path.join(
                    this.projectAbsolutePath,
                    this.folderNames.development.root,
                    this.folderNames.development.source.root,
                    this.folderNames.development.source.common.root,
                    this.folderNames.development.source.common.pug),

                PUG_OPEN_SOURCE_FILES_SELECTION_BASE = path.join(
                    this.projectAbsolutePath,
                    this.folderNames.development.root,
                    this.folderNames.development.source.root,
                    this.folderNames.development.source.open.root,
                    this.folderNames.development.source.open.pug),

                PUG_ADMIN_SOURCE_FILES_SELECTION = path.join(
                    this.projectAbsolutePath,
                    this.folderNames.development.root,
                    this.folderNames.development.source.root,
                    this.folderNames.development.source.admin.root,
                    this.folderNames.development.source.admin.pug),

                PUG_DEVELOPMENT_SOURCE_FILES_SELECTION = path.join(
                    this.projectAbsolutePath,
                    this.folderNames.development.root,
                    this.folderNames.development.source.root,
                    this.folderNames.development.source.development.root,
                    this.folderNames.development.source.development.pug);


        // SASSは、どちらがエントリポイントが、理解してるが、PUGなら、全部コンパイルしている
        this.pugSourceFilesSelection = {

            watchedFiles: [
                PUG_COMMON_SOURCE_FILES_SELECTION_BASE + '\\**\\*.pug',
                PUG_OPEN_SOURCE_FILES_SELECTION_BASE + '\\**\\*.pug',
                PUG_ADMIN_SOURCE_FILES_SELECTION + '\\**\\*.pug',
                PUG_DEVELOPMENT_SOURCE_FILES_SELECTION + '\\**\\*.pug'
            ],

            development: [
                PUG_OPEN_SOURCE_FILES_SELECTION_BASE + '\\*.pug',
                PUG_ADMIN_SOURCE_FILES_SELECTION + '\\*.pug',
                PUG_DEVELOPMENT_SOURCE_FILES_SELECTION + '\\*.pug'
            ]
        };

        this.htmlOutputPaths = {
            developmentBuild : {
                index: path.join(
                    this.projectAbsolutePath,
                    this.folderNames.development.root,
                    this.folderNames.development.devBuild.root
                ),
                open: path.join(
                    this.projectAbsolutePath,
                    this.folderNames.development.root,
                    this.folderNames.development.devBuild.root,
                    this.folderNames.development.devBuild.open.root
                ),
                admin: path.join(
                    this.projectAbsolutePath,
                    this.folderNames.development.root,
                    this.folderNames.development.devBuild.root,
                    this.folderNames.development.devBuild.admin.root
                ),
                development: path.join(
                    this.projectAbsolutePath,
                    this.folderNames.development.root,
                    this.folderNames.development.devBuild.root,
                    this.folderNames.development.devBuild.development.root
                )                    
            },
            productionBuild : {
                open: path.join(
                    this.projectAbsolutePath,
                    this.folderNames.production.root,
                    this.folderNames.production.open.root
                ),
                admin: path.join(
                    this.projectAbsolutePath,
                    this.folderNames.production.root,
                    this.folderNames.production.admin.root
                )
            }
        };


        this.htmlFilesSelectionForValidation = path.join(
                this.projectAbsolutePath,
                this.folderNames.development.root,
                this.folderNames.development.devBuild.root,
                this.folderNames.development.devBuild.open.root
            ) + '\\*.html';

        this.htmlFilesSelectionForCopyingToProduction = [
            path.join(
                this.projectAbsolutePath,
                this.folderNames.development.root,
                this.folderNames.development.devBuild.root,
                this.folderNames.development.devBuild.open.root
            ) + '\\*.html',
            path.join(
                this.projectAbsolutePath,
                this.folderNames.development.root,
                this.folderNames.development.devBuild.root,
                this.folderNames.development.devBuild.admin.root
            ) + '\\*.html'
        ];


        this.emailPugSourceFilesSelection = path.join(
                this.projectAbsolutePath,
                this.folderNames.development.root,
                this.folderNames.development.source.root,
                this.folderNames.development.source.admin.root,
                this.folderNames.development.source.admin.pug,
                'emails' + '\\**\\*.pug');

        this.emailInterimHtmlFilesSelction = path.join(
                this.projectAbsolutePath,
                this.folderNames.development.root,
                this.folderNames.development.devBuild.root,
                this.folderNames.development.devBuild.admin.root,
                'emails' + '\\**\\*.html');

        this.emailsHtmlFilesOutput = path.join(
                this.projectAbsolutePath,
                this.folderNames.development.root,
                this.folderNames.development.devBuild.root,
                this.folderNames.development.devBuild.admin.root,
                'emails'
        );


        // CSS関連

        const

            SASS_COMMON_SOURCE_FILES_SELECTION = path.join(
                this.projectAbsolutePath,
                this.folderNames.development.root,
                this.folderNames.development.source.root,
                this.folderNames.development.source.common.root,
                this.folderNames.development.source.common.sass
            ) + '\\**\\*.+(sass|scss)',

            SASS_OPEN_SOURCE_FILES_SELECTION = path.join(
                this.projectAbsolutePath,
                this.folderNames.development.root,
                this.folderNames.development.source.root,
                this.folderNames.development.source.open.root,
                this.folderNames.development.source.open.sass) + '\\**\\*.+(sass|scss)',

            SASS_ADMIN_SOURCE_FILES_SELECTION = path.join(
                this.projectAbsolutePath,
                this.folderNames.development.root,
                this.folderNames.development.source.root,
                this.folderNames.development.source.admin.root,
                this.folderNames.development.source.admin.sass) + '\\**\\*.+(sass|scss)',

            SASS_DEVELOPMENT_SOURCE_FILES_SELECTION = path.join(
                this.projectAbsolutePath,
                this.folderNames.development.root,
                this.folderNames.development.source.root,
                this.folderNames.development.source.development.root,
                this.folderNames.development.source.development.sass) + '\\**\\*.+(sass|scss)';

        this.sassSourceFilesSelection = {

            watchedFiles: [
                SASS_COMMON_SOURCE_FILES_SELECTION,
                SASS_OPEN_SOURCE_FILES_SELECTION,
                SASS_ADMIN_SOURCE_FILES_SELECTION,
                SASS_DEVELOPMENT_SOURCE_FILES_SELECTION
            ],
            development: [
                SASS_OPEN_SOURCE_FILES_SELECTION,
                SASS_ADMIN_SOURCE_FILES_SELECTION,
                SASS_DEVELOPMENT_SOURCE_FILES_SELECTION
            ],
            production: [
                SASS_OPEN_SOURCE_FILES_SELECTION,
                SASS_ADMIN_SOURCE_FILES_SELECTION
            ]
        };

        this.frameworkSassFilesSelection = path.join(
            this.projectAbsolutePath,
            this.folderNames.framework.root,
            this.folderNames.framework.styles
        ) + '\\*.sass';

        this.frameworkSassFilesDestination = {
            open: path.join(
                this.projectAbsolutePath,
                this.folderNames.development.root,
                this.folderNames.development.source.root,
                this.folderNames.development.source.open.root,
                this.folderNames.development.source.open.sass,
                'framework'
            ),
            admin: path.join(
                this.projectAbsolutePath,
                this.folderNames.development.root,
                this.folderNames.development.source.root,
                this.folderNames.development.source.admin.root,
                this.folderNames.development.source.admin.sass,
                'framework'
            ),
            development: path.join(
                this.projectAbsolutePath,
                this.folderNames.development.root,
                this.folderNames.development.source.root,
                this.folderNames.development.source.development.root,
                this.folderNames.development.source.development.sass,
                'framework'
            )
        };

        this.cssOutputPaths = {
            developmentBuild : {
                common: this.buildUncPathFromFromFolderNames([
                    this.folderNames.development.root,
                    this.folderNames.development.devBuild.root,
                    this.folderNames.development.devBuild.common.root,
                    this.folderNames.development.devBuild.common.css
                ]),
                open: this.buildUncPathFromFromFolderNames([
                    this.folderNames.development.root,
                    this.folderNames.development.devBuild.root,
                    this.folderNames.development.devBuild.open.root,
                    this.folderNames.development.devBuild.open.css
                ]),
                admin: this.buildUncPathFromFromFolderNames([
                    this.folderNames.development.root,
                    this.folderNames.development.devBuild.root,
                    this.folderNames.development.devBuild.admin.root,
                    this.folderNames.development.devBuild.admin.css
                ]),
                development: this.buildUncPathFromFromFolderNames([
                    this.folderNames.development.root,
                    this.folderNames.development.devBuild.root,
                    this.folderNames.development.devBuild.development.root,
                    this.folderNames.development.devBuild.development.css
                ])                    
            },
            productionBuild : {
                open: this.buildUncPathFromFromFolderNames([
                    this.folderNames.production.root,
                    this.folderNames.production.open.root,
                    this.folderNames.production.open.css
                ]),
                admin: this.buildUncPathFromFromFolderNames([
                    this.folderNames.production.root,
                    this.folderNames.production.admin.root,
                    this.folderNames.production.admin.css
                ])
            }
        };

        this.CSS_PRODUCTION_OUTPUT_FILES_SELECTION = [
            path.join(
                this.projectAbsolutePath,
                this.folderNames.production.root,
                this.folderNames.production.open.root,
                this.folderNames.production.open.css
            ) + '\\**\\*',
            path.join(
                this.projectAbsolutePath,
                this.folderNames.production.root,
                this.folderNames.production.admin.root,
                this.folderNames.production.admin.css
            ) + '\\**\\*'
        ];


        // JS 関連
        this.es6SourceFilesSelection = [
            this.buildUncPathFromFromFolderNames([
                this.folderNames.development.root,
                this.folderNames.development.source.root,
                this.folderNames.development.source.open.root,
                this.folderNames.development.source.open.es6
            ]) + '\\*.js',
            this.buildUncPathFromFromFolderNames([
                this.folderNames.development.root,
                this.folderNames.development.source.root,
                this.folderNames.development.source.admin.root,
                this.folderNames.development.source.admin.es6
            ]) + '\\*.js'
        ];

        this.JS_PRODUCTION_OUTPUT_FILES_SELECTION = [
            path.join(
                this.projectAbsolutePath,
                this.folderNames.production.root,
                this.folderNames.production.open.root,
                this.folderNames.production.open.js
            ) + '\\**\\*',
            path.join(
                this.projectAbsolutePath,
                this.folderNames.production.root,
                this.folderNames.production.admin.root,
                this.folderNames.production.admin.js
            ) + '\\**\\*'
        ];

        this.stringifiedEs6SourceFilesSelectionGlobPattern = `{${this.es6SourceFilesSelection.join(',')}}`;

        // PHP

        const

            PHP_OPEN_SOURCE_FILES_SELECTION_BASE = path.join(
                this.projectAbsolutePath,
                this.folderNames.development.root,
                this.folderNames.development.source.root,
                this.folderNames.development.source.open.root,
                this.folderNames.development.source.open.php),

           PHP_ADMIN_SOURCE_FILES_SELECTION = path.join(
                this.projectAbsolutePath,
                this.folderNames.development.root,
                this.folderNames.development.source.root,
                this.folderNames.development.source.admin.root,
                this.folderNames.development.source.admin.php);


        this.mockBackendPhpFilesSelection =  [
            PHP_OPEN_SOURCE_FILES_SELECTION_BASE + '\\**\\*.php',
            PHP_ADMIN_SOURCE_FILES_SELECTION + '\\**\\*.php'
        ];


        this.mockBackendFilesOutputPaths = {
            developmentBuild : {
                open: path.join(
                    this.projectAbsolutePath,
                    this.folderNames.development.root,
                    this.folderNames.development.devBuild.root,
                    this.folderNames.development.devBuild.open.root,
                    this.folderNames.development.devBuild.open.php
                ),
                admin: path.join(
                    this.projectAbsolutePath,
                    this.folderNames.development.root,
                    this.folderNames.development.devBuild.root,
                    this.folderNames.development.devBuild.admin.root,
                    this.folderNames.development.devBuild.admin.php
                )
            },
            productionBuild : {
                open: path.join(
                    this.projectAbsolutePath,
                    this.folderNames.production.root,
                    this.folderNames.production.open.root,
                    this.folderNames.production.open.php
                ),
                admin: path.join(
                    this.projectAbsolutePath,
                    this.folderNames.production.root,
                    this.folderNames.production.admin.root,
                    this.folderNames.production.admin.php
                )
            }
        };


        // 画像関連
        this.imagesSourceFilesSelection =              [
            path.join(    this.projectAbsolutePath,
                                this.folderNames.development.root,
                                this.folderNames.development.source.root,
                                this.folderNames.development.source.common.root,
                                this.folderNames.development.source.common.images) + '\\**\\*',

            path.join(    this.projectAbsolutePath,
                                this.folderNames.development.root,
                                this.folderNames.development.source.root,
                                this.folderNames.development.source.open.root,
                                this.folderNames.development.source.open.images) + '\\**\\*',

            path.join(    this.projectAbsolutePath,
                                this.folderNames.development.root,
                                this.folderNames.development.source.root,
                                this.folderNames.development.source.admin.root,
                                this.folderNames.development.source.admin.images) + '\\**\\*',

            path.join(    this.projectAbsolutePath,
                                this.folderNames.development.root,
                                this.folderNames.development.source.root,
                                this.folderNames.development.source.development.root,
                                this.folderNames.development.source.development.images) + '\\**\\*'
            ];

        this.imagesOutputPaths = {
            developmentBuild : {
                common: path.join(
                        this.projectAbsolutePath,
                        this.folderNames.development.root,
                        this.folderNames.development.devBuild.root,
                        this.folderNames.development.devBuild.common.root,
                        this.folderNames.development.devBuild.common.images),
                open: path.join(
                        this.projectAbsolutePath,
                        this.folderNames.development.root,
                        this.folderNames.development.devBuild.root,
                        this.folderNames.development.devBuild.open.root,
                        this.folderNames.development.devBuild.open.images),
                admin: path.join(
                        this.projectAbsolutePath,
                        this.folderNames.development.root,
                        this.folderNames.development.devBuild.root,
                        this.folderNames.development.devBuild.admin.root,
                        this.folderNames.development.devBuild.admin.images),
                development: path.join(
                        this.projectAbsolutePath,
                        this.folderNames.development.root,
                        this.folderNames.development.devBuild.root,
                        this.folderNames.development.devBuild.development.root,
                        this.folderNames.development.devBuild.development.images)
            },
            productionBuild : {
                common: path.join(
                    this.projectAbsolutePath,
                    this.folderNames.production.root,
                    this.folderNames.production.common.root,
                    this.folderNames.production.common.images),
                open: path.join(
                    this.projectAbsolutePath,
                    this.folderNames.production.root,
                    this.folderNames.production.open.root,
                    this.folderNames.production.open.images),
                admin: path.join(
                    this.projectAbsolutePath,
                    this.folderNames.production.root,
                    this.folderNames.production.admin.root,
                    this.folderNames.production.admin.images)
            }
        };
        
        
        // フォント関連
        this.fontsSourceFilesSelection =              [
            path.join(    this.projectAbsolutePath,
                                this.folderNames.development.root,
                                this.folderNames.development.source.root,
                                this.folderNames.development.source.common.root,
                                this.folderNames.development.source.common.fonts) + '\\**\\*',

            path.join(    this.projectAbsolutePath,
                                this.folderNames.development.root,
                                this.folderNames.development.source.root,
                                this.folderNames.development.source.open.root,
                                this.folderNames.development.source.open.fonts) + '\\**\\*',

            path.join(    this.projectAbsolutePath,
                                this.folderNames.development.root,
                                this.folderNames.development.source.root,
                                this.folderNames.development.source.admin.root,
                                this.folderNames.development.source.admin.fonts) + '\\**\\*',

            path.join(    this.projectAbsolutePath,
                                this.folderNames.development.root,
                                this.folderNames.development.source.root,
                                this.folderNames.development.source.development.root,
                                this.folderNames.development.source.development.fonts) + '\\**\\*'
            ];

        this.fontsOutputPaths = {
            developmentBuild : {
                common: path.join(
                        this.projectAbsolutePath,
                        this.folderNames.development.root,
                        this.folderNames.development.devBuild.root,
                        this.folderNames.development.devBuild.common.root,
                        this.folderNames.development.devBuild.common.fonts),
                open: path.join(
                        this.projectAbsolutePath,
                        this.folderNames.development.root,
                        this.folderNames.development.devBuild.root,
                        this.folderNames.development.devBuild.open.root,
                        this.folderNames.development.devBuild.open.fonts),
                admin: path.join(
                        this.projectAbsolutePath,
                        this.folderNames.development.root,
                        this.folderNames.development.devBuild.root,
                        this.folderNames.development.devBuild.admin.root,
                        this.folderNames.development.devBuild.admin.fonts),
                development: path.join(
                        this.projectAbsolutePath,
                        this.folderNames.development.root,
                        this.folderNames.development.devBuild.root,
                        this.folderNames.development.devBuild.development.root,
                        this.folderNames.development.devBuild.development.fonts)
            },
            productionBuild : {
                common: path.join(
                    this.projectAbsolutePath,
                    this.folderNames.production.root,
                    this.folderNames.production.common.root,
                    this.folderNames.production.common.fonts),
                open: path.join(
                    this.projectAbsolutePath,
                    this.folderNames.production.root,
                    this.folderNames.production.open.root,
                    this.folderNames.production.open.fonts),
                admin: path.join(
                    this.projectAbsolutePath,
                    this.folderNames.production.root,
                    this.folderNames.production.admin.root,
                    this.folderNames.production.admin.fonts)
            }
        };
    }
        
        
    buildUncPathFromFromFolderNames(pathSegmentsArray){

        let outputUNC_Path = '';
        let pathSegmentsArrayLength = pathSegmentsArray.length;

        for (let i=0; i < pathSegmentsArrayLength; i++) {

            if (i !== 0) {
                outputUNC_Path = outputUNC_Path + '\\' + pathSegmentsArray[i];
            }
            else {
                outputUNC_Path = outputUNC_Path + pathSegmentsArray[i];
            }
        }

        return outputUNC_Path;
    } 

    debug(){

    }
}

exports.HPath = new HPath();