'use strict';

const   gulp = require('gulp'),
            plugins = require('gulp-load-plugins')();

module.exports = options => {
    
    return () => {
        
        return gulp.src(options.HPath.htmlFilesSelectionForValidation)
        
            .pipe(plugins.plumber({
                    errorHandler: plugins.notify.onError( error => {
                        return {
                            title: 'Validate HTML: ',
                            message: error.message
                        };
                    })
            }))
            .pipe(plugins.htmlValidator({
                format: 'json'
            }))
            .pipe(plugins.intercept( file => {
                let json = JSON.parse(file.contents.toString());
                let errors = json.messages.filter((error, info, a) => {
                    return error.type !== 'info';
                });

                if (errors.length === 0) {
                    console.log('Валидация ' + file.basename + ': ошибок не обнаружено. Проверьте код на https://validator.w3.org/ на наличие предупреждений (warnings).');
                }
                else {
                    console.log(`Валидация ${file.basename} : обнаружено ошибок : ${errors.length}`);
                    console.log(errors);
                }
        }));
    };
};