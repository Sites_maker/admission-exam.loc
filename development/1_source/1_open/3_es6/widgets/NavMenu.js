export default class NavMenu {
   
   constructor () {
        
        document.getElementById('gamburger_menu_top').onclick=function(){
            var header_menu_top=document.getElementById('menu_top');
            if(header_menu_top.className==='')
                header_menu_top.className='sidebar';
                document.getElementsByTagName('body')[0].style.overflowY = 'hidden';
        }
        
        function closeSidebar(){
            var header_menu_top=document.getElementById('menu_top');
            if(header_menu_top.className==='sidebar')
                header_menu_top.className='';
            document.getElementsByTagName('body')[0].style.overflowY = 'auto';
        }  
        document.getElementById('close_menu_sidebar').onclick= closeSidebar;
        document.getElementById('sidebar_shading').onclick= closeSidebar;
        var handleMatchMedia = function(mediaQuery) {
            if (mediaQuery.matches) {
                    // если менее 570px или равное, то выполняется код между скобок 
            } else {closeSidebar();}// обратное условие, т.е если более 570px
        },
        mql = window.matchMedia('all and (max-width: 645px');
        handleMatchMedia(mql);
        mql.addListener(handleMatchMedia);
}
}  