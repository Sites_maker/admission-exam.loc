export default class FeedbackForm {
   
   constructor () {
        function showFeedbackForm(){
            $('div.FeedbackForm-RootElement').css('display','flex');
            document.getElementsByTagName('body')[0].style.overflowY = 'hidden';
        }
        $('header#menu_top>nav>ul>li>a#contacts').on('click',showFeedbackForm);
    }
}  