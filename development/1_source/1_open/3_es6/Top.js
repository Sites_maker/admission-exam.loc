import NavMenu from './widgets/NavMenu';
import HeroImageWithOffer from './01_Top/01_HeroImageWithOffer.js';
import GridCardView from './01_Top/04_GridCardView.js';
import FeedbackForm from './widgets/FeedbackForm.js'
(()=>{
    new NavMenu();
    new HeroImageWithOffer();
    new GridCardView();
    new FeedbackForm();
})();
