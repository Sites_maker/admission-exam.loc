export default class GridCardView {
   constructor () {
      const GRID_CARD_VIEW_WRAPPER_SELECTOR= '#Grid_card_view_wrapper';
      function showEditField(card_member_object){
          var members_card_id= card_member_object.parent().attr('id');
          $(GRID_CARD_VIEW_WRAPPER_SELECTOR+'>#'+members_card_id+'>div.team_members_popup_edit_delete').addClass('show_team_members_popup_edit_delete');
      }
      function showHideEditField(){
        var open_menu_member_cards_object= $('.show_team_members_popup_edit_delete');
        if(open_menu_member_cards_object.length==0){
            showEditField($(this));
        } else{
            open_menu_member_cards_object.removeClass('show_team_members_popup_edit_delete');
            
            if($(open_menu_member_cards_object[0]).parent().attr('id')!=$(this).parent().attr('id')){
                showEditField($(this));}
        }  
      }
        function deleteCard(){
            if($(this).parent().parent().hasClass('team_members'))
                $(this).parent().parent().remove();
        }
        function cutLongText(teg_p_class_team_member_about,text_length){
            if(teg_p_class_team_member_about.html()<text_length) return false;
            var text_Html_to_cut= teg_p_class_team_member_about.html(),
                short_string= text_Html_to_cut.substring(0,text_length),
                wheres_spacing = short_string.lastIndexOf('\u0020'),
                short_text;
            if(short_string[wheres_spacing-1]=='.' || short_string[wheres_spacing-1]==',' || short_string[wheres_spacing-1]=='-')
                wheres_spacing= wheres_spacing-1;
            short_text= short_string.substring(0,wheres_spacing);
            short_text= short_text + ' ...';      
            teg_p_class_team_member_about.html(short_text);
        }
        function editCard(){
            if(!$(this).parent().parent().hasClass('team_members')) return false;

            var members_card_id=$(this).parent().parent().attr('id'),
                p_class_team_member_about= $(GRID_CARD_VIEW_WRAPPER_SELECTOR+'>#'+members_card_id+'>p.team_member_about'),
                team_member_about_text= mas_text_html[members_card_id];
            p_class_team_member_about
                .html('<textarea rows="4" class= "allow_edit_team_member_about_text">'+team_member_about_text+'</textarea>')
                .addClass('allow_edit_team_member_about');
            $('.team_members_popup_edit_delete').removeClass('show_team_members_popup_edit_delete'); 
        }
   
        $(GRID_CARD_VIEW_WRAPPER_SELECTOR+'>div.team_members>svg.team_members_menu_open_close').on('click',showHideEditField);
        $('.team_members_popup_delete').on('click',deleteCard);
        $('.team_members_popup_edit').on('click',editCard);

        $(document).on('click',function(e){
            var target= e.target;
            if(!$(target).hasClass('team_members_menu_open_close') && !$(target).hasClass('show_team_members_popup_edit_delete') && $(target).parents('.show_team_members_popup_edit_delete').length==0)
                    $('.team_members_popup_edit_delete').removeClass('show_team_members_popup_edit_delete');
            if(!$(target).hasClass('team_members_menu_open_close') && !$(target).hasClass('allow_edit_team_member_about_text') && $(target).parents('.team_members_popup_edit_delete').length==0){
                if($('.allow_edit_team_member_about_text').length>0){
                    $('.allow_edit_team_member_about_text').each(function(){
                        $(this).parent().html($(this).html());
                    }); 
                    $('.team_member_about').removeClass('allow_edit_team_member_about');
                }    
            }
        })
        var p_class_team_member_about= $(GRID_CARD_VIEW_WRAPPER_SELECTOR+'>div.team_members>p.team_member_about');
        var mas_text_html=[];
        p_class_team_member_about.each(function(){
            mas_text_html[$(this).parent().attr('id')]=$(this).html(); 
            cutLongText($(this),81)});
   }
 }