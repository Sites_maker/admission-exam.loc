export default class HeroImageWithOffer {
   constructor () {
        const SLIDER_CONTAINER_SELECTOR= '#slider_container';
       
        function showNextSlider(slider_item_after){
            var id_link_after;
            if(!this)
                id_link_after= slider_item_after.attr('id');
            
            else id_link_after= $(this).attr('id');
            var 
                active_slider_before= $(SLIDER_CONTAINER_SELECTOR+'>div.slider_item.active_slider'),
                active_text_link_before=$(SLIDER_CONTAINER_SELECTOR+'>div.slider_text_link.active_text_link'),
                
                id_number= id_link_after[id_link_after.length-1],//до 10
               
                active_slider_after=$(SLIDER_CONTAINER_SELECTOR+'>#slider_'+id_number),
                active_text_link_after=$(SLIDER_CONTAINER_SELECTOR+'>#slider_text_link_'+id_number);          
                
                if(active_slider_before.attr('id')==active_slider_after.attr('id')) return false;
                active_slider_after.addClass('active_slider');
                active_text_link_after.addClass('active_text_link');
                
                if(active_slider_before.length){
                    active_slider_before.removeClass('active_slider');
                    active_text_link_before.removeClass('active_text_link');
                }
                
                $(SLIDER_CONTAINER_SELECTOR+'>#miniatur_sliders>a.miniatur_slider_item>img').css('border','2px solid transparent');
                $(SLIDER_CONTAINER_SELECTOR+'>#miniatur_sliders>a#miniatur_slider_'+id_number+'>img').css('border','2px solid orange');
        }

        var miniatur_sliders_link= $(SLIDER_CONTAINER_SELECTOR+'>#miniatur_sliders>a.miniatur_slider_item');
        for(var i=0; i< miniatur_sliders_link.length; i++){
            var id_miniatur_slider_active_after= 'miniatur_slider_' + (i+1);
            $(SLIDER_CONTAINER_SELECTOR+'>#miniatur_sliders>a#'+id_miniatur_slider_active_after).on('click',showNextSlider);
        } 
        
        function rotateSliders(){
            var 
                active_slider_before= $(SLIDER_CONTAINER_SELECTOR+'>div.slider_item.active_slider'),
                active_slider_after,
                id_after= 2;
            if(active_slider_before.length){
                var 
                    id_link_before= active_slider_before.attr('id'),
                    id_before= id_link_before[id_link_before.length-1],
                    id_after= Number(id_before)+1;
            }
            var all_sliders= $(SLIDER_CONTAINER_SELECTOR+'>div.slider_item');
            if(id_after<= all_sliders.length){
                active_slider_after= $(SLIDER_CONTAINER_SELECTOR+'>#slider_'+id_after);
            }
            else active_slider_after= $(SLIDER_CONTAINER_SELECTOR+'>#slider_1');
            showNextSlider(active_slider_after);
        }    
        setInterval(rotateSliders,3000);
   }
}  