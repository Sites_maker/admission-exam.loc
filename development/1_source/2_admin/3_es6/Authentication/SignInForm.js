let R;

export default class SignInForm {
        
    constructor(Resources){
        
        R = Resources;
        
        this.$SignInHtmlForm = $(R.ids.SignInForm.HtmlForm);
        
        this.$EmailInputField = this.$SignInHtmlForm.find(R.ids.SignInForm.InputFields.Email);
        this.$PasswordInputField = this.$SignInHtmlForm.find(R.ids.SignInForm.InputFields.Password);
        
        this.$EmailIsEmptyErrorMessage = this.$SignInHtmlForm.find(R.ids.SignInForm.ErrorMessages.EmptyEmail);
        this.$EmailIsInvalidErrorMessage = this.$SignInHtmlForm.find(R.ids.SignInForm.ErrorMessages.InvalidEmail);
        this.$PasswordIsEmptyErrorMessage = this.$SignInHtmlForm.find(R.ids.SignInForm.ErrorMessages.EmptyPassword);
        
        this.$SubmitButton = this.$SignInHtmlForm.find(R.ids.SignInForm.Buttons.SignIn);
        
        this.emailIsValid = false;
        this.passwordIsValid = false;
    }
    
    initialize(){
        this.initEmailInputField();
        this.initPasswordInputField();
        this.initSubmitButton();
    }
    
    
    initEmailInputField(){
        
        this.$EmailInputField.on('focusout', () => {
            this.validateEmail();
            this.manageSubmitButtonLocking();
        })
    }
    
    initPasswordInputField(){
        this.$PasswordInputField.on('input focusout', () => {
            console.log('done');
            this.validatePassword();
            this.manageSubmitButtonLocking();
        })
    }
    
    
    validateEmail(){
            
        let inputtedEmail = this.$EmailInputField.val();

        if (inputtedEmail.length === 0) {
            
            this.emailIsValid = false;
            
            this.$EmailInputField.addClass(R.cc.SignInForm.InputFields.statusModifiers.invalid);
            this.$EmailIsEmptyErrorMessage.slideDown('fast');
        }
        else {
            if (!/^(?:(?:[^<>()\[\]\.,;:\s@\"]+(?:\.[^<>()\[\]\.,;:\s@\"]+)*)|(?:\".+\"))@(?:(?:[^<>()\.,;\s@\"]+\.{0,1})+(?:[^<>()\.,;:\s@\"]{2,}|[\d\.]+))$/.test(inputtedEmail)) {
                
                this.emailIsValid = false;
                
                this.$EmailInputField.addClass(R.cc.SignInForm.InputFields.statusModifiers.invalid);
                this.$EmailIsInvalidErrorMessage.slideDown('fast');
                this.$EmailIsEmptyErrorMessage.slideUp('fast');
            }
            else {
                
                this.emailIsValid = true;

                this.$EmailIsEmptyErrorMessage.slideUp('fast');
                this.$EmailIsInvalidErrorMessage.slideUp('fast');
                this.$EmailInputField.removeClass(R.cc.SignInForm.InputFields.statusModifiers.invalid);
            }
        }
    }
    
    validatePassword(){
        
        let inputtedPassword = this.$PasswordInputField.val();

        if (inputtedPassword.length === 0) {
            
            this.passwordIsValid = false;
            
            this.$PasswordInputField.addClass(R.cc.SignInForm.InputFields.statusModifiers.invalid);
            this.$PasswordIsEmptyErrorMessage.slideDown('fast');
        }
        else {
            
            this.passwordIsValid = true;
            
            this.$PasswordInputField.removeClass(R.cc.SignInForm.InputFields.statusModifiers.invalid);
            this.$PasswordIsEmptyErrorMessage.slideUp('fast');
        }
    }
    
    
    manageSubmitButtonLocking(){
        
        if (this.emailIsValid && this.passwordIsValid) {
            this.$SubmitButton.prop('disabled', false);
        }
        else {
            this.$SubmitButton.prop('disabled', true);
        }
    }
    
    initSubmitButton(){
        
        this.$SubmitButton.on('click', event => {
            
            event.preventDefault();
            this.submitHtmlFormIfPermitted();
        });
    }
    
    submitHtmlFormIfPermitted(){
        
        if (this.emailIsValid && this.passwordIsValid) {
            this.$SignInHtmlForm.submit();
            this.setupSubmitButtonAppearanceAfterSubmitting();
        }
        else {
            return false;
        }
    }
    
    setupSubmitButtonAppearanceAfterSubmitting(){
        this.$SubmitButton
            .prop('disabled', true)
            .text(R.s.SignInForm.Messages.SigningIn);
    }
}