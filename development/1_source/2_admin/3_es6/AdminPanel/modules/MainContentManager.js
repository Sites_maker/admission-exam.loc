/* global IS_DEVELOPMENT */

let R;

export default class MainContentManager {
        
    static buildFragmentProductionLaravelRequest(fragmentIdInLaravelReferences){
        return `{{ route( REF::ADMIN_ROUTES("GetFragmentViaAjax"), ["fragment" => REF::ADMIN_PANEL_FRAGMENTS["${fragmentIdInLaravelReferences}"]["urnParameter"]]) }}`
    }
    
    static get FRAGMENTS(){ return {
            
        STATISTICS_TOP: {
            fragmentDevUrn: 'php/getViewTest.php',
            fragmentProdUrn: '/home/fragment/statistic_top',
            createInstance: () => {}
        },
        ALL_REQUESTS: {
            fragmentDevUrn: 'php/getViewTest.php',
            fragmentProdUrn: '/home/fragment/requests_all',
            createInstance: () => { }
        }
    }}

    static get DEFAULT_FRAGMENT() { return MainContentManager.FRAGMENTS.ALL_REQUESTS; }


    constructor(Resources){
      
        R = Resources;
        this.$MainContent = $(R.ccs.Layout.MainContent);
    }
    
    
    renderAndInitializeDefaultFragment(){
        this.renderAndInitializeNewFragment(MainContentManager.DEFAULT_FRAGMENT);
    }
    
    renderAndInitializeNewFragment(newFragment){
        
        let fragmentUrn = IS_DEVELOPMENT ? newFragment.fragmentDevUrn : newFragment.fragmentProdUrn;
        
        new Promise( (resolve, reject) => {
            
            this.$MainContent.load(fragmentUrn, (response, status, xhr) => {
                status === 'success' ?  resolve() : reject(xhr);
            });
        })        
                .then(() => {
                    //let newFragment = newFragment.createInstance();
                    //newFragment.initialize();
                })
                .catch((xhr) => {
                    console.error(`${R.s.ConsoleErrorMessages.GettingFragmentHtmlFailure} : ${xhr.status} ${xhr.statusText}`);
                });
    }
}