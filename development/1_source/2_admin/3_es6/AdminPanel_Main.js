/* global IS_DEVELOPMENT */

import LayoutManager from './AdminPanel/modules/LayoutManager';
import NavPanel from './AdminPanel/modules/NavPanel';
import MainContentManager from './AdminPanel/modules/MainContentManager';

import AdminPanelResources from './AdminPanel/resources';
const R = new AdminPanelResources();

(() => {
    
    new LayoutManager(R).setupLayout();
    
    this.mainContentManager =  new MainContentManager(R);
    if (!IS_DEVELOPMENT) {
        this.mainContentManager.renderAndInitializeDefaultFragment();
    }
    
    new NavPanel(this.mainContentManager, R);
    
})();