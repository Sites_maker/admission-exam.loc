import AuthenticationResources from './Authentication/resources';
const R = new AuthenticationResources();

import SignInForm from './Authentication/SignInForm';

(() => {
    executeRouting();
})();

function executeRouting(){
    
    let locationPathname = location.pathname;
    
    if (locationPathname.indexOf(R.routes.login.development) !== -1 ||
            locationPathname.indexOf(R.routes.login.production) !== -1) {
        new SignInForm(R).initialize();
    }
}